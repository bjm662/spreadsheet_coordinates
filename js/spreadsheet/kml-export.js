/***
 * Now takes user settings of column positions
 * We can get the location of relevant columsn by calling the cols global var
 *      eg cols.startDate (will be null if not set, else the number representing the column from a 0 based index)
 */

const header = `<?xml version="1.0" encoding="UTF-8"?>\n<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2">\n<Document>`;
const style = `    <Style id="journeyline">
		<IconStyle>
			<Icon/>
		</IconStyle>
		<LineStyle>
			<color>FF00FFFF</color>
			<width>2</width>
		</LineStyle>
	</Style>`;
const footer = `\n</Document>\n</kml>`

var prevMatches; //globally keep track of the last specified date REGEX MATCHES for journeys mode to compare to (as we can have date gaps)

//Track   (these need to be reset after each export)
var track, trackFooter, trackWhen, trackCoord

var ignoreErrors = false;

/**
 * Builds a KML output from the sheet and provides a download
 * Checks that dates are in a valid format
 * If Journeys mode is on, checks that dates are in chronological order
 *      Will stop and return error alert if either of the above fail
 * 
 * note: for Journeys mode we cannot have empty values for dates, so for now we will automatically copy the previous date into an empty column
 * @param {jExcel Sheet Object} mySheet 
 */
function exportKML(mySheet) {
    var data = mySheet.getData()
    prevMatches = null; //reset this on each iteration
    
    var output = header
	output += style
    track = `
    <Placemark>
		<styleUrl>#journeyline</styleUrl>
        <gx:Track>
			<extrude>1</extrude>                      
			<tessellate>1</tessellate>                
			<altitudeMode>clampToGround</altitudeMode>`;
    trackFooter = `
        </gx:Track>
    </Placemark>`
    trackWhen = ``;
    trackCoord = ``;

    if (isJourney && (!cols.startDate || !cols.endDate)) {
        alert('Journeys mode requires both a start and end date column to be specified.');
        return;
    }

    for (var row of data) {
        if (row[cols.startDate]) if (!ignoreErrors && (row[cols.startDate] = checkValidDate(row[cols.startDate])) == false) return false; //checkValidDate handles format validation, enforces chronological order on Journeys mode, AND alerts for both of these 
        if (row[cols.endDate]) if (!ignoreErrors && (row[cols.endDate] = checkValidDate(row[cols.endDate])) == false) return false; //Make sure we return if it comes back false, as this means there was an error and we do NOT want to export
        
        //if we have an empty start or end column value, copy the value over for now
        if (!row[cols.startDate] && row[cols.endDate]) row[cols.startDate] = row[cols.endDate];
        else if (row[cols.startDate] && !row[cols.endDate]) row[cols.endDate] = row[cols.startDate];

        //console.log(row[cols.startDate] + "  ---  " + row[cols.endDate])

        //Add placemark to the output string
        output += makePlacemark(row, mySheet)
    }
    if (isJourney) output += track + trackWhen + trackCoord + trackFooter
    output += footer;

    //Generate download file
    var blob = new Blob([output], {type: 'kml'})
    var url = window.URL.createObjectURL(blob)

    //Reset ignore errors
    ignoreErrors = false;

    //put it into an anchor, click it
    var link = document.createElement("a");    //do I need to delete this element afterward?
    link.download = "output.kml"
    link.href = url
    link.click()
    return true;
}

/**
 * Note, google kml uses longitude,latitude
 * @param {*} placename 
 * @param {*} latitude 
 * @param {*} longitude 
 * @param {*} description 
 */
function makePlacemark(row, sheet) {
    var placename = row[cols.placename] || '';//if undefined or null just use empty string
    var latitude = row[cols.latitude];
    var longitude = row[cols.longitude];
    var startDate = row[cols.startDate];
    var endDate = row[cols.endDate];
    var description = row[cols.description] || ''; //if undefined or null just use empty string
    if (!latitude) latitude = 0
    if (!longitude) longitude = 0

var datahtmltable = "";
if (exportExtendedData) {
        datahtmltable += `
        <table>`
        for (var i=0; i<row.length; i++) {
            datahtmltable += `
			<tr><td>${sheet.getHeader(i)}</td><td>${row[i]}</td></tr>
            `
        }
        datahtmltable += `
        </table>`
    }
description = description + datahtmltable; //add the datahtml table

    var str = '';
    if (placename) str += `
    <Placemark>
        <name><![CDATA[${placename}]]></name>`
    if (description) str += `
        <description><![CDATA[${description}]]></description>`
    str += `
        <Point>
            <coordinates>${longitude},${latitude},0</coordinates>
        </Point>`
    if (startDate) {
        str += `
        <TimeSpan>
            <begin>${startDate}</begin>
            <end>${endDate}</end>
        </TimeSpan>`
    }

    if (exportExtendedData) {
        str += `
        <ExtendedData>`
        for (var i=0; i<row.length; i++) {
            str += `
            <Data name="${sheet.getHeader(i)}">
                <value><![CDATA[${row[i]}]]></value>
            </Data>`
        }
        str += `
        </ExtendedData>`
    }

    //in Journey mode, add tracks
    if (isJourney && startDate && endDate) {
        makeTrack(placename,latitude,longitude,startDate,endDate)
    }
    str += `
    </Placemark>`
    return str
}

function makeTrack(placename,latitude,longitude,startDate,endDate) {
    trackWhen += `
            <when>${startDate}</when>
            <when>${endDate}</when>`
    trackCoord += `
            <gx:coord>${longitude} ${latitude} 0</gx:coord>
            <gx:coord>${longitude} ${latitude} 0</gx:coord>`
}

/**
 * Return true if valid or false if invalid format
 * USES GLOBAL VARIABLES isJourney and prevEnd?
 * regex: '/^(-?[0-9]*[1-9]+0*)(-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])(T(0?[0-9]|1[0-9]|2[0-3])(:(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])(:(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])([.][0-9]+)?)?)?)?)?$/'
 * OR
 * '/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])\/(-?[0-9]*[1-9]+0*)( ((0?[0-9]|1[0-9]|2[0-3]):(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])))?$/'
 * 
 * See dateRegexCompare() for more info on accepted formats
 * @param {string} dateString 
 */
function checkValidDate(dateString) {
    var matches = dateString.match(/^(-?[0-9]*[1-9]+0*)(-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])(T(0?[0-9]|1[0-9]|2[0-3])(:(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])(:(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])([.][0-9]+)?)?)?)?)?$/);
    var needsConvert = false;
    if (!matches) {
        matches = dateString.match(/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])\/(-?[0-9]*[1-9]+0*)( ((0?[0-9]|1[0-9]|2[0-3]):(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])))?$/); 
        needsConvert = true;
    }

    //Error Case 1: does not match
    if (!matches) { //if no match and ignoreErrors isn't true show an error, if in journey show error regardless of ignoreErrors value
        if (!ignoreErrors && !isJourney) {
            if (confirm("Date '"+dateString+"' is an invalid date format!\nYou may continue but this KML will be invalid with some TLCMap systems!")) {
                ignoreErrors = true
            } else return false;
        }
        else if (isJourney) {alert("Date '"+dateString+"' is an invalid date format!\nYou cannot export invalid dates in Journeys mode.\nPlease fix this or disable Journeys mode in the exporter settings."); return false;}
        else return false;
    }
    
    //If we are on Journeys mode we must compare the current date with the last date referenced, it MUST be either equal to or greater than this prevDate!
    if (isJourney) {
        if (prevMatches) { 
            //Error Case 2: journeys mode is on and this date is BEFORE the previous date 
            if (dateRegexCompare(matches,prevMatches) == -1) {
                alert("Date '"+dateString+"' is chronologically BEFORE the previous date '"+prevMatches[0]+"' but is listed after it on the spreadsheet which is not allowed in Journeys mode.\nPlease fix this by putting the dates in the correct order or disable Journeys mode in the exporter settings.");
                return false;
            }
        }
        prevMatches = matches; //overwrite previous matches with current matches for comparison with the next date value
    }
    
    if (needsConvert && !ignoreErrors) { //Convert to ISO Friendly date string
        return (matches[7] != undefined) ? matches[3]+"-"+matches[2]+"-"+matches[1]+"T"+matches[6]+":"+matches[7] : matches[3]+"-"+matches[2]+"-"+matches[1];
    }

    return dateString;
}

/**
 * Function to compare dates of various accepted formats. This does NOT change the input dates at all.
 * 
 * regex: '/^(-?[0-9]*[1-9]+0*)(-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])(T(0?[0-9]|1[0-9]|2[0-3])(:(0?[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])(:(0?[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])([.][0-9]+)?)?)?)?)?$/'
 * OR
 * '/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])\/(-?[0-9]*[1-9]+0*)( ((0?[0-9]|1[0-9]|2[0-3]):(0?[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])))$/'
 * 
 * Will read 2 digit dates as current century, eg 01/01/21 is Jan 1st 2021
 *  If the user wants to specify an actual 2 digit date use leading zeroes, eg 0021 for the year 21CE
 * 
 * IMPORTANT NOTE: 
 *  * accepted formats (6 ISO friendly, 2 Excel friendly):
 *      - Year  (accepts negatives, leading zeroes, 1 to n characters, cannot be 0)                             Results in an array of size 2: [0] is full string, [1] is Year
 *      - Year-Month-Day (as above, and day/month cannot be single digits... eg: 1993-02-12 not 1993-2-12)      Results in an array of size 5: [1] is Year, [3] is Month, [4] is Day
 *      - Year-Month-DayThh      AS above but with time in hours                                                Results in an array of size 7: [1] is Year, [3] is Month, [4] is Day, [6] is Hour
 *      - Year-Month-DayThh:mm      AS above but with time in hours and minutes                                 Results in an array of size 9: [1] is Year, [3] is Month, [4] is Day, [6] is Hour, [8] is minute
 *      - Year-Month-DayThh:mm:ss (as above but with time)                                                      Results in an array of size 11: [1] is Year, [3] is Month, [4] is Day, [6] is Hour, [8] is Minute, [10] is Second
 *      - Year-Month-DayThh:mm:ss.sss (as above but with decimal seconds)                                       Results in an array of size 12: [1] is Year, [3] is Month, [4] is Day, [6] is Hour, [8] is Minute, [10] is Second, [11] is decimal seconds
 *      - Day/Month/Year (year month and day rules as above)                                                    Results in an array of size 4: [1] is Day [2] is Month, [3] is Year
 *      - Day/Month/Year hh:mm (as above but with time, no seconds as this is how excel exports csv)            Results in an array of size 8: [1] is Day, [2] is Month, [3] is Year, [6] is Hour, [7] is Minute
 *      
 */
function dateRegexCompare(a,b) {
    //console.log(a)
    //console.log(b)
    if (a.length == 4) a = [null,a[3],null,a[2],a[1]]; //convert (local copy of date) to the same array format as ISO yyyy-mm-dd
    if (b.length == 4) b = [null,b[3],null,b[2],b[1]];
    if (a.length == 8) a = [null,a[3],null,a[2],a[1],null,a[6],null,a[7]]; //convert (local copy of date) to same array format as ISO yyyy-mm-ddThh:mm
    if (b.length == 8) b = [null,b[3],null,b[2],b[1],null,b[6],null,b[7]]; 
    if (a[1].length == 2) a[1] = "20" + a[1]; //if we only specify a 2 digit year we should assume they mean this decade, eg 01/01/21 is Jan 1st 2021
    if (b[1].length == 2) b[1] = "20" + b[1]; //if we only specify a 2 digit year we should assume they mean this decade, eg 01/01/21 is Jan 1st 2021

    for (var i=1; i < 12; i++) {
        if (i==2 || i==5 || i==7 || i==9) continue; //skip these, they have irrelevant values
        if (!a[i] && b[i]) return -1; //if b specifies to more accuracy then it comes after a, eg: 2011 comes before 2011-12-31
        if (!b[i] && a[i]) return 1;
        if (parseFloat(a[i]) < parseFloat(b[i])) return -1; //if year is BEFORE or AFTER the compare value, we don't need to bother checking month, day, etc
        if (parseFloat(a[i]) > parseFloat(b[i])) return 1; 
    }
    return 0; //if we got this far ALL of the values are EQUAL to one another
}