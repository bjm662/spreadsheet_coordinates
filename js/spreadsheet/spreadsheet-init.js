//CONSTS
const cellColor = '#666';
const altCellColor = '#777';

//IMPORTANT VARS
var mySheet; //the jexcel object
var row=0; //rows are ZERO-BASED INDEXED (ie spreadsheet A1 is 0,0)

//The table column indexes for every important column - It is important that 
var cols = {placename: 0, latitude: 1, longitude: 2, startDate: null, endDate: null, description: null} //assign these to point to relevant columns, check on read-in, check on add date button, check on export?

//Global vars to keep track of checkbox values
var isJourney = false
var exportExtendedData = false;
var infillDates = false;

//Defaults for reversion (when we switch to a new table?)
var default_cols = {...cols}

//Doc elements
var sheetdiv = document.getElementById('sheetdiv');
var sheet = document.getElementById('sheet');
var nosheet = document.getElementById('nosheet');
var newsheetbutton = document.getElementById('newsheetbutton');
var loadsheetbutton = document.getElementById('loadsheetbutton');
var uploadbutton = document.getElementById('uploadbutton')
var uploadFile = document.getElementById("uploadFile");

var opensheet = document.getElementById('opensheet');
var savesheetbutton = document.getElementById('savesheetbutton');
var cancelsheetbutton = document.getElementById('cancelsheetbutton');

var helpmodal = document.getElementById('helpmodal');
var settingsModal = document.getElementById('settingsModal')
var savesettingsbutton = document.getElementById('savesettingsbutton')

var placenameinput = document.getElementById('placenameinput');
var latitudeinput = document.getElementById('latitudeinput');
var longitudeinput = document.getElementById('longitudeinput');
var startdateinput = document.getElementById('startdateinput');
var enddateinput = document.getElementById('enddateinput');
var journeycheckbox = document.getElementById('journeycheckbox')

//this is the proper js naming convention, we should change everything over to this in the future
var dateSettingsModal = document.getElementById('date-settings-modal');
var saveDateSettingsButton = document.getElementById('save-date-settings-button'); 
var addDateColumnsButton = document.getElementById('add-date-columns-button'); 
var infillDatesCheckbox = document.getElementById('infill-dates-checkbox');




/**************************/
/*    TABLE DEFAULTS      */
/**************************/
var default_headers = ['placename','latitude','longitude'];
var default_data = [
    ['Some Place','123.45','45.66'],
    ['Elsewhere','',''],
    ['Unknown','',''],
];
const original_headers = ['placename','latitude','longitude'];
const original_data = [
    ['Some Place','123.45','123.45'],
    ['Elsewhere','',''],
    ['Unknown','',''],
];

/**************************/
/*     TABLE TOOLBAR      */
/**************************/
var toolbar = [
    {
        type: 'i',
        content: 'undo',
        onclick: function() {
            mySheet.undo();
        }
    },
    {
        type: 'i',
        content: 'redo',
        onclick: function() {
            mySheet.redo();
        }
    },
    {
        type: 'i',
        content: 'save',
        onclick: function () {
            if (checkDates()) mySheet.download(true);
        }
    },
    {
        type: 'i',
        content: 'south',
        onclick: function () {
            showsettings();
            //moving exportKML to the settings modal save button
        }
    },
    {
        type: 'i',
        content: 'calendar_today',
        onclick: function () {
            showDateSettings();
            //addStartEndDate();
            //$(mySheet.toolbar).children().eq(4).remove() //remove button from toolbar
        }
    },
    {
        type: 'i',
        content: 'add_link',
        onclick: function () {
            addLinkback();
            $(mySheet.toolbar).children().eq(5).remove() //remove button from toolbar
        }
    },
    {
        type: 'i',
        content: 'help',
        onclick: function () {
            showhelp();
        }
    },
    {
        type: 'i',
        content: 'delete_forever',
        onclick: function () {
            deletesheet();
        }
    },
];

/**************************/
/*       NEW SHEET        */
/**************************/
/**
 * Creates a sheet from the given data
 * @param {*} headers 
 * @param {*} data 
 */
function newsheet(headers,data) {
    mySheet = jexcel(sheet, {
        data: data,
        colHeaders: headers,
        csvHeaders: true,
        columns: [
            { type:'text', width:120 },
            { type:'text', width:120 },
            { type:'text', width:120 }
        ],
        toolbar: toolbar,
        tableOverflow: false,
        updateTable:function(instance,cell,col,row,val,label,cellName) {
            if (row % 2) cell.style.backgroundColor = cellColor;
            else cell.style.backgroundColor = altCellColor;
        },
        onselection:function(table,startCol,startRow,endCol,endRow) {
            row = startRow;
            updateMsgbox("Editing coordinates for <strong>" + 
                mySheet.getValueFromCoords(cols.placename,row) //coords uses 0-based indexing
                    + "</strong> in row <strong>" + (row+1) + "</strong>"); //spreadsheet uses 1-based indexing
        },
        onfocus:function(instance) {

        },
        onblur:function(instance) {
            reselectRow()
        },
        onundo:function(instance) {
            reselectRow()
        },
        onredo:function(instance) {
            reselectRow()
        },
        oninsertcolumn:function(instance,col_index,b,c,before) { //col_index refers to the original column index, before is true if we inserted the new column before this else false, unsure what b and c are
            var index = col_index + !before
            for (var prop in cols) {
                if (cols[prop] !== null && cols[prop] >= index) cols[prop]++
            }
            resetAllSettings();
        },
        ondeletecolumn:function(instance,col_index,b,c) { //col_index is the index of the deleted column
            for (var prop in cols) {
                if (cols[prop] !== null) {
                    if (cols[prop] == col_index) cols[prop] = null
                    else if (cols[prop] > col_index) cols[prop]--
                }
            }
            resetAllSettings();
        }
    })

    togglebuttons();

    //TODO: Edit content of toolbar to include a CLEAN looking csv and kml identifier text
    $('div.jexcel_toolbar i:nth-child(1)').attr({ "data-placement": "bottom", title:"Undo"});
    $('div.jexcel_toolbar i:nth-child(2)').attr({ "data-placement": "bottom", title:"Redo"});
    $('div.jexcel_toolbar i:nth-child(3)').attr({ "data-placement": "bottom", title:"Save as CSV"});
    $('div.jexcel_toolbar i:nth-child(4)').attr({ "data-placement": "bottom", title:"Export as KML"});
    $('div.jexcel_toolbar i:nth-child(5)').attr({ "data-placement": "bottom", title:"Add Date columns"});
    $('div.jexcel_toolbar i:nth-child(6)').attr({ "data-placement": "bottom", title:"Add a 'linkback' column to include links back to your relevant pages"});
    $('div.jexcel_toolbar i:nth-child(7)').attr({ "data-placement": "bottom", title:"Help"});
    $('div.jexcel_toolbar i:nth-child(8)').attr({ "data-placement": "bottom", title:"Delete and return to menu"});
    
    //default select first row
    var firstrow = mySheet.getRowData(0);
    mySheet.updateSelectionFromCoords(0,0,firstrow.length-1,0); //this will go back and call the onselection function
}

/**************************/
/*       FUNCTIONS        */
/**************************/
/**
 * Select the next row if it exists
 */
function nextRow() {
    row++
    var thisrow = mySheet.getRowData(row)
    if (thisrow != undefined) {
        mySheet.updateSelectionFromCoords(0,row,thisrow.length-1,row); //Table has an on selection function that will automatically update the row variable to the next row!
    }
    else { //FINISHED!
        mySheet.resetSelection();
        updateMsgbox("Finished! Don't forget to save!");
    }
}

/**
 * Some actions will deselect the spreadsheet row when we don't want them to, so we use this to reselect the current row
 */
function reselectRow(){
     if (mySheet != undefined) {
        var thisrow = mySheet.getRowData(row)
        if (thisrow != undefined) {
            mySheet.updateSelectionFromCoords(0,row,thisrow.length-1,row);
        }
     }

}

/**
 * Toggle between spreadsheet loader menu and active spreadsheet content
 */
function togglebuttons() {
    $(sheet).toggleClass('d-none');
    $(nosheet).toggleClass('d-none');
}

/**
 * Deletes the current spreadsheet
 */
function deletesheet() {
    if ( confirm("This will delete the spreadsheet from the browser (will not affect local files). \nAre you sure you wish to do this?") ) {
        cols = {...default_cols}
        resetAllSettings();
        togglebuttons(); //show the mainscreen buttons
        jexcel.destroy(document.getElementById('sheet'), false);
        default_data = original_data; //reset the data object
        default_headers = original_headers; //reset the data object
        mySheet = null; //unassign the jexcel object (so we can recreate it)
        $(sheet).remove(); //delete the sheet div
        var newsheet = $("<div id=\"sheet\" class=\"d-none\"></div>")
        $(sheetdiv).prepend(newsheet);
        sheet = document.getElementById('sheet');
        updateMsgbox("Please open a spreadsheet to begin using tool")
    }
}

/**
 * Show the help modal - a popup with user help resources
 */
function showhelp() {
    $(helpmodal).modal('show');
}

/**
 * Read a csv or txt file as parameter, extract relevant data, forward to spreadsheet creator function
 * @param {*} file 
 */
function importFile(file) {
    if (file) {
        var ext = file.name.split('.')[1] //file extension
        if (ext != 'txt' && ext != 'csv') {
            alert('Error loading file. Is the file a .txt or .csv file? Is the file valid csv? Check the Help section for more info.')
            return
        }

        //read file - jexcel cannot handle a raw file from file picker so we must read content into array
        var reader = new FileReader();
        reader.readAsText(file, "UTF-8")
        reader.onload = function (evt) {
            var data = $.csv.toArrays(evt.target.result)
            newsheet(data[0],data.slice(1)) //headers and data
        }    
    }
}

function addStartEndDate() {
    //jexcel add 2 columns
    //create custom column types according to specs https://bossanova.uk/jexcel/v4/examples/column-types
    mySheet.insertColumn(2, 999999, 0, [{title: 'datestart', width:100, type: 'text', editor:BCColumn},{title: 'dateend', width: 100, type: 'text', editor:BCColumn}]) //# of columns, col index to place at n, (0 for aftern or 1 for before n), single or array of object properties
    var len = mySheet.getRowData(0).length
    cols.startDate = len-2
    cols.endDate = len-1
    resetAllSettings();
}

function addLinkback() {
    //jexcel add 1 column
    //create custom column types according to specs https://bossanova.uk/jexcel/v4/examples/column-types
    mySheet.insertColumn(1, 999999, 0, {title: 'linkback', width:100, type: 'text', editor:linkbackColumn}); //# of columns, col index to place at n, (0 for aftern or 1 for before n), single or array of object properties
}

function showsettings() {
    $(settingsModal).modal("show")
}

function showDateSettings() {
    $(dateSettingsModal).modal("show");
}

function savesettings() {
    cols.placename = (placenameinput.value != "") ? placenameinput.value : null
    cols.latitude = (latitudeinput.value != "") ? latitudeinput.value : null
    cols.longitude = (longitudeinput.value != "") ? longitudeinput.value : null
    cols.description = (descriptioninput.value != "") ? descriptioninput.value : null
    exportExtendedData = extendeddatacheckbox.checked
    isJourney = journeycheckbox.checked
}

function saveDateSettings() {
    cols.startDate = (startdateinput.value != "") ? startdateinput.value : null;
    cols.endDate = (enddateinput.value != "") ? enddateinput.value : null;
    infillDates = infillDatesCheckbox.checked;

    //Some magic to make the columns switch to the date picker style and make the old ones switch back to text style


}

function resetDateSettings() {
    startdateinput.value = cols.startDate;
    enddateinput.value = cols.endDate;
    infillDatesCheckbox.checked = infillDates;
}

function resetAllSettings() {
    resetsettings();
    resetDateSettings();
}

function resetsettings() {
    placenameinput.value = cols.placename
    latitudeinput.value = cols.latitude
    longitudeinput.value = cols.longitude
    descriptioninput.value = cols.description
    journeycheckbox.checked = isJourney
}

/**
 * returns true if date is a valid format, else false
 * 
 * regex: '/^(-?[0-9]*[1-9]+0*)(-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])(T(0?[0-9]|1[0-9]|2[0-3])(:(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])(:(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])([.][0-9]+)?)?)?)?)?$/'
 * OR
 * '/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])\/(-?[0-9]*[1-9]+0*)( ((0?[0-9]|1[0-9]|2[0-3]):(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])))?$/'
 * 
 * IMPORTANT NOTE: 
 *  * accepted formats (6 ISO friendly, 2 Excel friendly):
 *      - Year  (accepts negatives, leading zeroes, 1 to n characters, cannot be 0)                             Results in an array of size 2: [0] is full string, [1] is Year
 *      - Year-Month-Day (as above, and day/month cannot be single digits... eg: 1993-02-12 not 1993-2-12)      Results in an array of size 5: [1] is Year, [3] is Month, [4] is Day
 *      - Year-Month-DayThh      AS above but with time in hours                                                Results in an array of size 7: [1] is Year, [3] is Month, [4] is Day, [6] is Hour
 *      - Year-Month-DayThh:mm      AS above but with time in hours and minutes, minutes must be 2 digits       Results in an array of size 9: [1] is Year, [3] is Month, [4] is Day, [6] is Hour, [8] is minute
 *      - Year-Month-DayThh:mm:ss (as above but with time)      seconds must be 2 digits                        Results in an array of size 11: [1] is Year, [3] is Month, [4] is Day, [6] is Hour, [8] is Minute, [10] is Second
 *      - Year-Month-DayThh:mm:ss.sss (as above but with decimal seconds)                                       Results in an array of size 12: [1] is Year, [3] is Month, [4] is Day, [6] is Hour, [8] is Minute, [10] is Second, [11] is decimal seconds
 *      - Day/Month/Year (year month and day rules as above)                                                    Results in an array of size 4: [1] is Day [2] is Month, [3] is Year
 *      - Day/Month/Year hh:mm (as above but with time, no seconds as this is how excel exports csv)            Results in an array of size 8: [1] is Day, [2] is Month, [3] is Year, [6] is Hour, [7] is Minute  
 */    
function validateDate(dateString) {
    return (dateString.match(/^(-?[0-9]*[1-9]+0*)(-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])(T(0?[0-9]|1[0-9]|2[0-3])(:(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])(:(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])([.][0-9]+)?)?)?)?)?$/)
        || dateString.match(/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])\/(-?[0-9]*[1-9]+0*)( ((0?[0-9]|1[0-9]|2[0-3]):(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])))?$/))  
        ? true : false;
}

function checkDates() {
    var data = mySheet.getData();
    var len = data.length;
    var fail = false;

    if (cols.startDate || cols.endDate) {
        for (var i = 0; i < len; i++) { //for each row in sheet
            if (cols.startDate) {
                if (data[i][cols.startDate] != '' && !validateDate(data[i][cols.startDate])) {
                    fail = true;
                    //console.log(data[i][cols.startDate])
                    if (confirm('Invalid date format in row '+(i+1)+' column '+mySheet.getHeader(cols.startDate)+'\nIgnore all date format errors and download anyway?')) return true;
                    break;
                }
            }
            if (cols.endDate) {
                if (data[i][cols.endDate] != '' && !validateDate(data[i][cols.endDate])) {
                    fail = true;
                    //console.log(data[i][cols.endDate])
                    if (confirm('Invalid date format in row '+(i+1)+' column '+mySheet.getHeader(cols.endDate)+'\nIgnore all date format errors and download anyway?')) return true;
                    break;
                }
            }
        }
    }
    return !fail; //return true if success, return false if we failed
}

function validateLink(str) {
    console.log('Validate Link function called')
    return str.match(/https?:\/\/(www\.)?.+\..+/i);
}

//Custom column type for Date values
var BCColumn = {
    // Methods
    closeEditor : function(cell, save) {
        //console.log('closeEditor called');
        var value = cell.children[0].value;
        cell.innerHTML = value;
        return value;
   },
    openEditor : function(cell) {
        //console.log('openEditor called');
        // Create input
        var element = document.createElement('input');
        element.value = cell.innerHTML;
        // Update cell
        cell.classList.add('editor');
        cell.innerHTML = '';
        cell.appendChild(element);
        $(element).datepicker({
            format: 'yyyy-mm-dd',  
            forceParse: false,
            autoclose: true,
            keyboardNavigation: false,
            afterHide:function() { 
                setTimeout(function() {
                    // To avoid double call
                    if (cell.children[0]) {
                        mySheet.closeEditor(cell, true);
                    }
                });
            }
        })
        .on('changeDate', function(e) {
            //console.log('dateChanged');
            if (cell.children[0]) {
                //console.log('double check called');
                mySheet.closeEditor(cell, true);
            } else {
                cell.innerHTML = e.format();
                mySheet.setValue(cell, e.format());
            }
        });
        $(element).on("keydown", function(e){
            if (e.key == "Enter" || e.key=="Tab") {
                jexcel.current.closeEditor(jexcel.current.edition[0], true);
                $(element).datepicker("hide");
            }
        });
        // Focus on the element
        element.focus();
        element.select();
   },
    getValue : function(cell) {
        //console.log('getValue called');
        return $(cell).html();
    },
    setValue : function(cell, value) {
        //console.log('setValue called');
        $(cell).html(value);
        mySheet.setValue(cell, value.format())
        //return true;
    }
}

var linkbackColumn = {
    // Methods
    closeEditor : function(cell, save) {
        var value = cell.children[0].value;

        //validate url
        var isValid = validateLink(value);

        //make the text red if invalid or green if valid
        $(cell).removeClass("validLink invalidLink");

        if (isValid)  $(cell).addClass("validLink");
        else  $(cell).addClass("invalidLink");

        //Set value
        cell.innerHTML = value;
        return value;
   },
    openEditor : function(cell) {
        // Create input
        var element = document.createElement('input');
        element.value = cell.innerHTML;

        // Update cell
        cell.classList.add('editor');
        cell.innerHTML = '';
        cell.appendChild(element);
        
        //Enter or Tab enters the input and moves on
        $(element).on("keydown", function(e){
            if (e.key == "Enter" || e.key=="Tab") {
                jexcel.current.closeEditor(jexcel.current.edition[0], true);
            }
        });

        // Focus on the element
        element.focus();
        element.select();
   },
    getValue : function(cell) {
        return $(cell).html();
    },
    setValue : function(cell, value) {
        $(cell).html(value);
        mySheet.setValue(cell, value.format())
    }
}


/**************************/
/*         EVENTS         */
/**************************/

/*  HOME SCREEN BUTTONS   */
$(newsheetbutton).on('click', function() {
    newsheet(default_headers, default_data);
})
$(loadsheetbutton).on('click', function() {
    uploadFile.click(); //fires $(uploadFile).change event after file select
})

/*      FILE SELECTED     */
$(uploadFile).change(function() {
    var fileToLoad = uploadFile.files[0];
    importFile(fileToLoad)
    $(uploadFile)[0].value = '';
    //$(settingsModal).modal("show")
})

/*  SETTINGS MODAL HIDDEN   */
$(settingsModal).on('hide.bs.modal', function(e) {
    resetsettings()
})

/*  SETTINGS MODAL SAVE   */
$(savesettingsbutton).on('click', function() {
    var len = mySheet.getRowData(0).length
    var input_arr = [placenameinput.value, latitudeinput.value, longitudeinput.value]
    var fail = false
    for (var item of input_arr) {
        if (item !== "" && (item >= len || input_arr.filter(x => x==item).length > 1)) {fail = true; break;}  //if non-empty input is outside of spreadsheet OR already taken
    }
    if (fail) alert('One of these values is invalid')
    else {
        savesettings()
        updateMsgbox("Settings saved!")
        if (exportKML(mySheet)) { //moving date validation functionality to kml exporter tool to avoid multiple loops
            $(settingsModal).modal("hide") //only hide if we successfully exported the kml
        } 
        
    }
})

$(saveDateSettingsButton).on('click', function() {
    saveDateSettings();
    updateMsgbox("Date settings saved!");
    $(dateSettingsModal).modal("hide");
})

$(addDateColumnsButton).on('click', function() {
    addStartEndDate();
    $(dateSettingsModal).modal("hide");
})


//https://stackoverflow.com/questions/469357/html-text-input-allow-only-numeric-input
//https://jsfiddle.net/emkey08/zgvtjc51

// Restricts input for the given textbox to the given inputFilter.
function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    });
  }
//ensure settings box inputs are only ints >= 0
setInputFilter(placenameinput, function(value) {
    return /^\d*$/.test(value); });
setInputFilter(latitudeinput, function(value) {
    return /^\d*$/.test(value); });
setInputFilter(longitudeinput, function(value) {
    return /^\d*$/.test(value); });
setInputFilter(startdateinput, function(value) {
    return /^\d*$/.test(value); });
setInputFilter(enddateinput, function(value) {
    return /^\d*$/.test(value); });
    