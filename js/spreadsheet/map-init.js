/**************************/
/*         CONSTS         */
/**************************/
const maxImages = 3
const msgboxdiv = document.getElementById('msgboxdiv');
const msgbox = document.getElementById('msgbox'); //span object containing descriptive text
const coordsmsg = document.getElementById('coords'); //span object containing descriptive text
const uploadImage = document.getElementById('uploadImage'); //input to upload a file to be used as a map overlay

const mapOptions = {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 20,
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYmVub3oxMSIsImEiOiJjazNpMmsyeGIwM3ZnM2JwaW9mdG9sdWl1In0.RrwSfVxBLJhqSK3aTsEaNw'
}

/**************************/
/*          VARS          */
/**************************/
var useDecimals = true
var clickable = true
var imgOpacitySlider
var lat,lng
var mySheet
var startX, startY
var images = []
var lastclicked //the last clicked button, we use this to only update the button state IFF an image upload is selected
var lastselected //the last selected image, this is the one that will be referred to by the toolbars (eg opacity slider)
var imgbuttons = [] //holds the image overlay buttons

/**************************/
/*       MAP SETUP        */
/**************************/
var mymap = L.map('mapid', { center: new L.LatLng(-32.8945, 151.6976), zoom: 7 } ); //default to Newcastle coordinates

/* Tile Layers */
var street = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token={accessToken}', mapOptions);

var satellite = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/{z}/{x}/{y}?access_token={accessToken}', mapOptions);

var hybrid = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/tiles/{z}/{x}/{y}?access_token={accessToken}', mapOptions);

var street_no_labels = L.tileLayer('https://api.mapbox.com/styles/v1/benoz11/ckdb6jpdj1v3x1inyajccx0uy/tiles/{z}/{x}/{y}?access_token={accessToken}', mapOptions);

var light = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/light-v10/tiles/{z}/{x}/{y}?access_token={accessToken}', mapOptions);

var light_no_labels = L.tileLayer('https://api.mapbox.com/styles/v1/benoz11/ckdb77jh21vo31is9lglj9llg/tiles/{z}/{x}/{y}?access_token={accessToken}', mapOptions);

/* Add layers to map */
L.control.layers(
    {
        "Street + Labels": street.addTo(mymap),
        "Street": street_no_labels,
        "Hybrid": hybrid,
        "Satellite": satellite,
        "Light + Labels": light,
        "Light": light_no_labels
    }, 
    {  }, 
    { position: 'topleft', collapsed: true }
).addTo(mymap);

L.control.scale({position: "bottomright"}).addTo(mymap);

/* Marker layer for Gazetteer searching */
var gazetteerLayer = L.featureGroup().addTo(mymap);

/* Custom Marker */
var dotIcon = L.icon({
    iconUrl: 'icons/blueDot.png',
    shadowUrl: 'icons/blueDotShadow.png',
    iconSize: [11,11],
    shadowSize: [13,13],
    iconAnchor: [6,6],
    shadowAnchor: [7,7],
    popupAnchor: [-300,-300]
})

/**************************/
/*       MAP Buttons      */
/**************************/

/* Button for toggling between decimal and degrees */
var decimalToggleButton = L.easyButton({
    states: [{
        stateName: 'decimal',
        icon: '<i class="material-icons">toggle_off</i>',
        title: 'Switch to degrees, minutes, and seconds',
        onClick: function(btn,map) {useDecimals = false; btn.state('degree')}
    }, {
        stateName: 'degree',
        icon: '<i class="material-icons">toggle_on</i>',
        title: 'Switch to decimals',
        onClick: function(btn,map) {useDecimals = true; btn.state('decimal')}
    }]
}).addTo(mymap);

/* Button for adding an image overlay */
imgbuttons.push(newImageButton())

/* Control bar to hold the image overlay buttons */
var imgbuttonbar = L.easyBar(imgbuttons).addTo(mymap);

/* Overlay Image Distortion Toolbar buttons */
var toolbarbuttons = [
    L.easyButton('<img src="./icons/drag.svg">', function() {lastselected.editing.setMode('drag')}),
    L.easyButton('<img src="./icons/scale.svg">', function() {lastselected.editing.setMode('scale')}),
    L.easyButton('<img src="./icons/distort.svg">', function() {lastselected.editing.setMode('distort')}),
    L.easyButton('<img src="./icons/rotate.svg">', function() {lastselected.editing.setMode('rotate')}),
    L.easyButton('<img src="./icons/crop_rotate.svg">', function() {lastselected.editing.setMode('freeRotate')}),
    L.easyButton('<img src="./icons/lock.svg">', function() {lastselected.editing._toggleLockMode()}),
    L.easyButton('<img src="./icons/border_outer.svg">', function() {lastselected.editing._toggleBorder()}),
    L.easyButton('<img src="./icons/get_app.svg">', function() {exportImage()}), //the built in exporter doesnt work properly, call our own export function
    L.easyButton('<img src="./icons/delete_forever.svg">', function() {lastselected.remove()})
]

/* Overlay Image Distortion Toolbar */
var overlaytoolbar = L.easyBar(toolbarbuttons,{position: "topright"}).addTo(mymap);
$( overlaytoolbar.container ).hide(); //hide initially

/**************************/
/*  IMAGE OPACITY SLIDER  */
/**************************/
/* Extend the control object with a custom opacity slider control */
L.Control.ImgOpacitySlider = L.Control.extend({
    onAdd: function(map) {
      var el = L.DomUtil.create('input');
      el.type="range";
      el.title="Image overlay opacity"
      el.id="imgOpacitySlider";
      el.class=""
      el.hidden = true //initially hidden, show when an image is added
      el.value="100"
      //el.innerHTML = '<input type="range" min="1" max="100" value="100" id="imgOpacitySlider" class="easy-button-button leaflet-bar-part leaflet-interactive decimal-active">';

      el.onmouseover = function() { disableMapControls() }
      el.onmouseout = function() { enableMapControls() }

      el.oninput = function(e) {
        if (lastselected) $( lastselected._image ).css( { opacity: e.target.value/100 } ) //change opacity of overlay image
      }
      imgOpacitySlider = el
      return el;
    },
  
    onRemove: function(map) {
      // Nothing to do here
    }
});

/* Define the creation function for the custom opacity slider control */
L.control.imgOpacitySlider = function(opts) { return new L.Control.ImgOpacitySlider(opts); }
  
L.control.imgOpacitySlider({ position: 'topright' }).addTo(mymap);

/**************************/
/*        EVENTS          */
/**************************/
/**
 * When mouse is clicked on map: 
 *      Get coordinates from click location
 *      Update coordinates on spreadsheet
 *      Proceed to next row
 */
mymap.on('click', () => {mapclick()}); 

/**
 * When focus changes to map:
 *      Add an event listener on spacebar down to skip to the next row
 */
mymap.on('focus', function(e) {
    if (mySheet != undefined) {
        mymap.addEventListener('keydown', function(e) { //When spacebar is pressed, go to next row in spreadsheet
            if (e.originalEvent.code === 'Space') {
                nextRow()
            }
        })
    }
})

/**
 * When focus changes off of map:
 *      Remove the spacebar down event listener
 */
mymap.on('blur', function(e) { //When we unfocus from map, delete the spacebar event
    mymap.removeEventListener('keydown')
})

/**
 * When mouse moves over map:
 *      Get mouse coordinates and display them in bottom left
 */
mymap.addEventListener('mousemove', function(e) {
    const c = getCoords(e.latlng)
    lat = c.lat
    lng = c.lng
    coordsmsg.innerHTML = lat + ", " + lng; //update the bottom message to display hover coordinates
})

/**
 * When mouse moves off map:
 *      Remove mouse coordinates from bottom left
 */
mymap.on('mouseout', function(e) {
    coordsmsg.innerHTML = "";
})

/**
 * FILE SELECTED from file picker
 */
$(uploadImage).change(function() {
    var imgToLoad = uploadImage.files[0]; //get first file selected
    var success = importImage(imgToLoad); //call our import image function
    if (success) {
        lastclicked.state('select'); //change the state of the button that was clicked
        if (images.length < maxImages) addButton() //add another 'add image' button if we havent hit the maximum yet
    } else {alert('error loading image')}
    $(uploadImage)[0].value = ''; //reset value so we may upload the same image twice if we choose to do so
})

$("#gazetteer-search").on('click',searchGazetteer);

$("#gazetteer-input").on('keypress', function(e) {
    if (e.key == 'Enter') {
        searchGazetteer();
    }
})


/**************************/
/*       FUNCTIONS        */
/**************************/

/**
 * Search the gazetteer and put results onto map
 */
function searchGazetteer(e){
    var input = $("#gazetteer-input").val();
    var selectVal = $("#gazetteer-select option:selected").val();
    //var url = "/test/newcastle.json"; //Test value for dev server
    var url = "https://tlcmap.org/ghap/search?"+selectVal+"="+input+"&format=json"; //Use this on live server
    const request = new Request(url, {method: 'GET'});
    console.log(url);

    $.ajax({
        dataType: "json",
        url: url,
        success: function(data) {
            gazetteerLayer.clearLayers();
            L.geoJson(data, {
                pointToLayer: function(feature, latlng) {
                    return L.marker(latlng, {icon: dotIcon});
                },
                onEachFeature: function (feature, layer) {
                    layer.bindTooltip(
                        '<p class="popupName">'+feature.properties.name+'</p><p class="popupLatlng">'+ feature.geometry.coordinates[1] + "," + feature.geometry.coordinates[0] +'</p>'
                    );
                  }
            }).addTo(gazetteerLayer);
        }
    });
}

/**
 * Event that takes place on a mapclick - filtered out into its own function so it may be called from a SIMULATED click as well
 */
function mapclick() {
    if (!!clickable) {
        if (mySheet != undefined) {
            if (cols.latitude !== null && cols.longitude !== null) { //if the spreadsheet has lat/long columns defined
                //SET LAT/LONG ON SPREADSHEET - lat lng vars previously set by mousemove event
                var thisrow = mySheet.getRowData(row)
                if (thisrow != undefined) {
                    mySheet.setValueFromCoords(cols.latitude,row,lat)
                    mySheet.setValueFromCoords(cols.longitude,row,lng)
                    // mySheet.setValue('B'+(row+1), lat); //add 1 to row, as B1 is row 0
                    // mySheet.setValue('C'+(row+1), lng);
                }
            
                //Select the next row if it exists
                nextRow()
            } else {alert('Please use the spreadsheet settings to designate a latitude and longitude column!'); $(settingsModal).modal('show')}
        }
        else { //if no spreadsheet loaded, allow coordinates to clipboard mode! - Copy coordinates of mouse click position to clipboard
            var cin = document.createElement("INPUT"); //new temporary input
            cin.setAttribute("type", "text");
            cin.value = coordsmsg.innerHTML; //set value to lat/long
            document.body.appendChild(cin);
            cin.select();
            cin.setSelectionRange(0, 99999); /*For mobile devices*/
            document.execCommand("copy");
            updateMsgbox('Copied <strong>' + cin.value + '</strong> to clipboard');
            cin.remove();
        }
    } 
}

/**
 * Takes a click or hover event on the leaflet map
 * Only works if event concerns the leaflet map, as we call e.latlng
 * Performs all relevant conversions, wrapping, etc
 * @param {event} e 
 */
function getCoords(coord) {
    var coord = coord.wrap() //wrap() wraps the coordinates around, so 190 longitude returns -170 instead
    var lat = coord.lat.toFixed(8); //limit to 8 dec pl
    var lng = coord.lng.toFixed(8);

    //convert to degrees if user desires it
    if (!useDecimals) {
        lat = decimalToDegrees(lat)
        lng = decimalToDegrees(lng)
    }
    return {lat:lat,lng:lng}
}

function updateMsgbox(contents, flashcolor="#feff9c", flashtime=1000) {
    msgbox.innerHTML = contents

    //set new css
    $(msgboxdiv).css("background-color", flashcolor)

    //fade back to old
    $(msgboxdiv).stop(true,true).animate({backgroundColor: "white"}, flashtime)
}

/**
 * Creates a distorable image overlay
 * @param {*} file 
 */
function importImage(file) {
    var count = images.length 
    if (count < maxImages) {
        if (file) {
            var img = L.distortableImageOverlay(
                URL.createObjectURL(file), 
                { actions: [L.DragAction, L.ScaleAction, L.DistortAction, L.RotateAction, L.FreeRotateAction, L.LockAction, L.BorderAction, L.ExportAction, L.DeleteAction], suppressToolbar:true }
            ).addTo(mymap); 
            images.push(img)//add to the layerGroup
            imgOpacitySlider.hidden = false //no longer need to hide the slider
            $( overlaytoolbar.container ).show()

            img.addEventListener('remove', function(e) {
                images = images.filter(function(value,index,arr) {return value != img})
                removeButton()
                if (!selectOverlayImageLayer(images[0])) {
                    imgOpacitySlider.hidden = true //if the layer exists select it, if it doesn't exist hide the image overlay opacity slider
                    $( overlaytoolbar.container ).hide()
                }
            })
            
            img._image.addEventListener('mousedown', function (e) { //if we mousedown on the overlay image
                if (img.editing._mode == 'lock') { //and the mode is in LOCK
                    startX = e.clientX
                    startY = e.clientY
                }
            })
    
            img._image.addEventListener('mouseup', function (e) { //if we mouseup event without moving on overlay image
                if (img.editing._mode == 'lock') { //and the mode is in LOCK
                    if (Math.abs(startX - e.clientX) < 5 && Math.abs(startY - e.clientY) < 5) { //no movement (4 pixels of movement allowed)
                        mapclick()
                    }
                }
                lastselected = img  //if image is clicked we set this image to be the one toolbars refer to
                $( imgOpacitySlider ).val( $( lastselected._image ).css('opacity') * 100 )
            })

            selectOverlayImageLayer(img)

            return true

        } else {return false}
    } else {return alert('Reached image overlay limit!')} //shouldn't ever reach this
  
}

/**
 * https://stackoverflow.com/questions/5786025/decimal-degrees-to-degrees-minutes-and-seconds-in-javascript
 * 
 * Separated by colon by default, but you can input other symbols as you wish
 */
function decimalToDegrees(degree, degSymbol=":", minSymbol = ":", secSymbol="") {
    var h = (degree > 0) ? 1 : -1 //get the hemisphere
    var deg = Math.abs(degree) //remove the pos/neg component
    var d = Math.floor(deg) //degrees
    var minfloat = (deg-d)*60
    var m = Math.floor(minfloat) //minutes
    var secfloat = (minfloat-m)*60
    var s = secfloat.toFixed(2) //seconds to 2 dec places
    if (s >= 60 && s <= 61) {
        console.log("rare seconds rounding case!");
        m++;
        s=s-60;
    }
    if (m==60) {
        d++;
        m=0;
    }
    return ("" + (h * d) + degSymbol + m + minSymbol + s + secSymbol); //reapply the pos/neg component but only to the degree portion
}

function disableMapControls() {
    //console.log('map drag disabled')
    mymap.dragging.disable();
    mymap.touchZoom.disable();
    mymap.doubleClickZoom.disable();
    mymap.scrollWheelZoom.disable();
    mymap.boxZoom.disable();
    mymap.keyboard.disable();
    if (mymap.tap) mymap.tap.disable();
    clickable=false;
}

function enableMapControls() {
    //console.log('map drag enable')
    mymap.dragging.enable();
    mymap.touchZoom.enable();
    mymap.doubleClickZoom.enable();
    mymap.scrollWheelZoom.enable();
    mymap.boxZoom.enable();
    mymap.keyboard.enable();
    if (mymap.tap) mymap.tap.enable();
    clickable=true;
}

function addButton() {
    imgbuttons.push(newImageButton()) //add the button to the array
    imgbuttonbar.remove() //remove the bar that holds the buttons
    imgbuttonbar = L.easyBar(imgbuttons).addTo(mymap); //remake and readd the bar to the map
}

function removeButton() {
    var count = images.length //get count of how many images are on the map
    imgbuttons = [] //reset image button array
    for (var i=0; i<count; i++) { //for each image slot still taken, add an image button in select mode
        var newbtn = newImageButton()
        newbtn.state('select') //put into select mode
        imgbuttons.push(newbtn)
    }
    imgbuttons.push(newImageButton()) //we now have at least 1 free image slot, so add an image button in upload mode
    imgbuttonbar.remove()
    imgbuttonbar = L.easyBar(imgbuttons).addTo(mymap); //remake and readd the bar to the map
}

/**
 * Functions as a template for an image overlay button
 *      If isFirst
 * @param {*} isFirst 
 */
function newImageButton(btnarr=imgbuttons) {
    var ind = btnarr.length
    return L.easyButton({
        states: [{
                stateName: 'upload',        // name the state
                icon:      '<i class="material-icons">add_photo_alternate</i>',               // and define its properties
                title:     'Add an overlay image',      // like its title
                onClick: function(btn, map) {       // and its callback
                    uploadImage.click()
                    lastclicked = btn
                }
            }, {
                stateName: 'select',
                icon:      '<i class="material-icons" style="color:#0075ff">image</i>',
                title:     'Select overlay image',
                onClick: function(btn, map) {
                    selectOverlayImageLayer(images[ind])
                }
        }]
    })
}

/**
 * Select the given image overlay map layer and adjusts the relevant vars and tools
 *      
 * @param {*} layer 
 */
function selectOverlayImageLayer(layer) {
    if (layer) {
        layer.select() //L.DistortableImage function to select the image (brings up toolbar, etc)
        lastselected = layer //set the last selected to be this layer (for other functions)

        $( imgOpacitySlider ).val( $( lastselected._image ).css('opacity') * 100 ) //adjust the opacity
        return true
    } return false
}

/**
 * Custom export function as the built in one is buggy
 */
function exportImage() {
    //lastselected.editing._getExport() //this is the default export function, which doesn't seem to work properly!
    var overlay = lastselected.editing._overlay;
    var map = overlay._map;
    var img = overlay.getElement();


    // make a new image
    var downloadable = new Image();

    downloadable.id = downloadable.id || 'tempId12345';
    document.body.appendChild(downloadable);

    downloadable.onload = function onLoadDownloadableImage() {
      var height = downloadable.height;
      var width = downloadable.width;
      var nw = map.latLngToLayerPoint(overlay.getCorner(0));
      var ne = map.latLngToLayerPoint(overlay.getCorner(1));
      var sw = map.latLngToLayerPoint(overlay.getCorner(2));
      var se = map.latLngToLayerPoint(overlay.getCorner(3));

      // run once warping is complete (THIS NEVER FIRES?)
        //   downloadable.onload = function() {
        //     L.DomUtil.remove(downloadable);
        //   };

      if (window && window.hasOwnProperty('warpWebGl')) {
        warpWebGl(
            downloadable.id,
            [0, 0, width, 0, width, height, 0, height],
            [nw.x, nw.y, ne.x, ne.y, se.x, se.y, sw.x, sw.y],
            true // trigger download
        );
      }

      L.DomUtil.remove(downloadable); //added this to remove it from the page

    };

    downloadable.src = overlay.options.fullResolutionSrc || img.src;

}

/**
 * Debug method to print all of the available methods for a given object
 *      A life saver when working with poorly documented scripts and APIs 
 * @param {*} obj 
 */
function getMethods(obj) {
    let properties = new Set()
    let currentObj = obj
    do {
      Object.getOwnPropertyNames(currentObj).map(item => properties.add(item))
    } while ((currentObj = Object.getPrototypeOf(currentObj)))
    return [...properties.keys()].filter(item => typeof obj[item] === 'function')
  }