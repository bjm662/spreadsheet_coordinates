const mapOptions = {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 20,
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYmVub3oxMSIsImEiOiJjazNpMmsyeGIwM3ZnM2JwaW9mdG9sdWl1In0.RrwSfVxBLJhqSK3aTsEaNw'
}

var mymap = L.map('mapid', { center: new L.LatLng(-32.8945, 151.6976), zoom: 7 } ); //default to Newcastle coordinates

/* Tile Layers */
L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token={accessToken}', mapOptions).addTo(mymap);

/* Scale widget */
L.control.scale({position: "topright"}).addTo(mymap);


/* Marker layer group */
var markerLayer = L.layerGroup()

/**************************/
/*       FUNCTIONS        */
/**************************/
function loadPlaces (places) {
    for (place of places) {
        var startDate = (place.startDate === undefined) ? undefined : place.startDate.toString() //setting time variable to the startDate if exists, set undefined to 0000 to aid in sorting
        var endDate = (place.endDate === undefined) ? undefined : place.endDate.toString()
        var marker = L.marker([place.geo.latitude, place.geo.longitude], {color: 'red', startDate: startDate, timeStrLength: 4, alwaysShowDate: true, endDate: endDate})
        marker.bindPopup(place.name + "<br><br>" + place.description + "<br><br>latitude: " + place.geo.latitude + "<br>longitude: " + place.geo.longitude)
        marker.addTo(markerLayer)
    }
    
    //time slider
    var sliderControl = L.control.sliderControl({position: "bottomleft", layer: markerLayer, timeStrLength: 4, timeAttribute: 'startDate', range: true, clusters: true, noDatesCheckbox: false, isEpoch: true}); //calls the initialize function
    mymap.addControl(sliderControl); //Adds the slider to the map, calls the onAdd function
    sliderControl.startSlider(); //calls the startSlider function
}
