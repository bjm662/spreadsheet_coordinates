/**************************/
/*      CONSTS & VARS     */
/**************************/
const uploadFile = document.getElementById('uploadFile');
const loadBtn = document.getElementById('load-button');

var places = [
    {
        name: "some place 1",
        startDate: "11-JUN-2020",
        geo: {latitude: -32.8945, longitude: 151.6976}
    },
    {
        name: "some place 2",
        startDate: "12-JUN-2020",
        geo: {latitude: -33.8945, longitude: 151.6976}
    },
    {
        name: "some place 3",
        startDate: "13-JUN-2020",
        geo: {latitude: -34.8945, longitude: 151.6976}
    }
]

/**************************/
/*         EVENTS         */
/**************************/
/* Load json/kml button clicked */
$(loadBtn).on('click', function() {
    uploadFile.click(); //fires $(uploadFile).change event after file select
})

/* File Selected */
$(uploadFile).change(function() {
    var fileToLoad = uploadFile.files[0];
    importFile(fileToLoad)
})


/**************************/
/*       FUNCTIONS        */
/**************************/
function importFile(file) {
    if (file) {
        var ext = file.name.split('.')[1] //file extension
        var fileReader = new FileReader();
        fileReader.onload = async (e) => {
            if (ext == 'json' || ext == 'geojson') { loadPlaces(await this.extractJSON(e.target.result)) }
            else if (ext == 'kml') { loadPlaces(await this.extractKML(e.target.result)) }
            else {
                error()
                return
            }
            $( loadBtn ).hide()
        }
        fileReader.readAsText(file)
    }
}

/**
 * https://stackoverflow.com/questions/18032505/read-out-kml-file-with-javascript
 * @param {*} plainText 
 */
function extractKML(plainText) {
    let parser = new DOMParser()
    let xmlDoc = parser.parseFromString(plainText, "text/xml")
    let places = []
    let count = 0 

    if (xmlDoc.documentElement.nodeName == "kml") {
        for (const item of xmlDoc.getElementsByTagName('Placemark')) {
            let name = item.getElementsByTagName('name')[0]
            if (name !== undefined) name = name.childNodes[0]           //error handling for missing tag
            name = (name !== undefined) ? name.nodeValue.trim() : ""    //error handling for empty tag

            let markers = item.getElementsByTagName('Point')

            let description = item.getElementsByTagName('description')[0]
            if (description !== undefined) description = description.childNodes[0]          //error handling for missing tag
            description = (description !== undefined) ? description.nodeValue.trim() : "No description"   //error handling for empty tag

            /* MARKER PARSE */   
            for (const marker of markers) {
                count++
                var coords = marker.getElementsByTagName('coordinates')[0]
                if (coords !== undefined) coords = coords.childNodes[0]                             //error handling for missing tag
                var coord = (coords !== undefined) ? coords.nodeValue.trim().split(",") : [0,0,0]   //error handling for empty tag (marker @ coordinates [0,0,0])
                places.push(
                    { 
                        name: name,
                        description: description,
                        startDate: count,
                        geo: {latitude: coord[1], longitude: coord[0]}
                    }
                )
            }
        }
    } else { throw "error while parsing" }

    return places
}

function extractJSON(plainText) {
    let geoJSON = JSON.parse(plainText)
    let features = geoJSON.features
    let places = []
    let count = 0 

    /* MARKER PARSE */
    for (const feature of features) {

        if (feature.geometry.type === "Point") {
            count++
            var coord = feature.geometry.coordinates
            var name = ''
            var description = ''
            places.push(
                { 
                    name: name,
                    description: description,
                    startDate: count,
                    geo: {latitude: coord[1], longitude: coord[0]}
                }
            )
        }
    }
    return places
}


function error() {
    alert('Error loading file. Is it the correct file type (.kml .json or .geojson)? Is it valid kml/geojson?')
}