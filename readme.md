# TLCMap Spreadsheet-Coordinate Tools
A standalone web tool for populating a spreadsheet of locations with their coordinate locations.

## Primary Functions

###### Coordinates to Clipboard
With no spreadsheet loaded a user can copy coordinates straight to their clipboard by clicking anywhere on the map.

###### Spreadsheet Tool
User can quickly populate a spreadsheet by clicking on the map to get the coordinates.

## Basic Usage
User can create a new spreadsheet or upload an existing one.

Select the row to edit, then click anywhere on the map to input the coordinates for this placename from the map to the spreadsheet.

Spreadsheet will automatically progress to the next row.

Spreadsheet functions similarly to other spreadsheet software, such as Microsoft Excel.

Pressing SPACEBAR while focused on the map will skip to the next row in the spreadsheet.

Click the Layers icon on the map to change the map style.

Click the Image icon on the map to upload an image overlay. This can be used to overlay an existing historical map to assist in the process.

Click the Toggle icon on the map to switch between decimal and degree formats for coordinates.


## Requirements
Spreadsheet requires at least 3 columns: placename, latitude, longitude.
CSV files imported into the system must have column names in the first row.
System currently assumes column 1 is placename, column 2 is latitude, column 3 is longitude.

## About

Created by Benjamin McDonnell for [TLCMap](http://tlcmap.org)